<?php

    # LOOPS - Executes a block of code a set number of times

    /**
     * - For Loop
     * - While Loop
     * - Do...While Loop
     * - Foreach Loop
     */

     # For Loop
     # @params = init, condition, increment/decrement

    //  for($i=0;$i < 10; $i++){
    //      echo $i . "<br>";
     
    //    }

    # While Loop
    # @params = condition

    // $j = 10;
    // while($j > 0){
    //     echo $j . "<br>";
    //     $j--;
    // }

    # Do While Loop
    # @params = condition

    // $k = 10;
    // do{
    //     echo $k . "<br>";
    //     $k++;
    // }while($k < 10);

    # Foreach Loop  (Single Array)
    // $people = ['Kevin','John','Martin','Tom'];

    // foreach($people as $person){
    //     echo $person . "<br>";
    // }

    # Foreach Loop (Associative Array)
    $people = ['Brad' => 'brad23@gmail.com','John' => 'john24@outlook.com','Tom' => 'tom34@example.com'];

    foreach($people as $person => $email){
        echo $person . ": " . $email;
        echo "<br>";
    }









