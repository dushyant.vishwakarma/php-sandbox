<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <script>
        function showSuggestion(str) {
  if (str.length == 0) {
    document.getElementById("output").innerHTML = "";
  } else {
    // AJAX Request
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById("output").innerHTML = this.response;
      }
    };
    xmlhttp.open("GET", "suggestions.php?q=" + str, true);
    xmlhttp.send();
  }
}
    </script>
    <title>Search User</title>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-primary"> 
    <div class="container">
        <div class="navbar-header">
            <a href="index.php" class="navbar-brand">My Website</a>
        </div>
    </div>
</nav>

<div class="container">
    <div style="margin-top:30px">

    </div>