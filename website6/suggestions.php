<?php

$people[] = "Henry";
$people[] = "John";
$people[] = "Mark";
$people[] = "Vikash";
$people[] = "Rajesh";
$people[] = "Harshit";
$people[] = "Nishchay";
$people[] = "Usha";
$people[] = "Brad";
$people[] = "Brock";
$people[] = "Evans";
$people[] = "Abdul";
$people[] = "Rumman";
$people[] = "Kanchan";

// Get Query String
$q = $_REQUEST['q'];

$suggestion = '';

// Get Suggestions
if($q != ''){
    $q = strtolower($q);
    $len = strlen($q);

    foreach($people as $person){
        if(stristr($q,substr($person,0,$len))){
            if($suggestion === ""){
                $suggestion = $person;
            } else {
                $suggestion .= ",$person";
            }
        }
    }
}

echo $suggestion === "" ? "No Suggestion" : $suggestion;