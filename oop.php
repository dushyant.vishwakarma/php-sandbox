<?php
    // Create a class
    class Person {
        private $name;
        private $email;
        public static $age_limit = 21;      // static property

        // Creating a constructor
        public function __construct($name,$email){
            $this->name = $name;
            $this->email = $email;

            // Magic property __CLASS__ gives the name of the class
            echo __CLASS__ . " Object Created! " . "<br>";
        }

        // Creating a destructor
        public function __destruct(){
            echo __CLASS__ . " Object Destroyed!<br>";
        }

        public function get_email(){
            return $this->email . "<br>";
        }

        public function get_name(){
            return $this->name . "<br>";
        }

        // A static function
        public static function get_age_limit(){
            // in static methods we dont use $this, we use self::property_name
            return self::$age_limit;
        }

    }

    // Inheritance
    class Customer extends Person {
        private $balance;

        public function __construct($name,$email,$balance){
            parent::__construct($name,$email,$balance);
            $this->balance = $balance;
            echo __CLASS__ . " Object Created!<br>";
        }

        public function __destruct(){
            echo __CLASS__ . " Object Destroyed!<br>";
        }


        public function get_balance(){
            return $this->balance . "<br>";
        }

    }

    // instantiating a class
    // $person1 = new Person("Heath Ledger","iamheath@ledger.com");
    // $person2 = new Person("Christian Bale","christian@bale.com");

    // $person->name = "John Doe";

    // echo $person->name;

    // $person1->set_name("Mark Hamill");

    // echo $person1->get_name();

    // echo $person1->get_name();
    // echo $person1->get_email();

    // $customer1 = new Customer("Rajesh Thakur","rajesh@thakur.com",4000);

    // echo $customer1->get_balance();

    // Accessing static properties without instantiating the class
    // echo Person::$age_limit;
    echo Person::get_age_limit();