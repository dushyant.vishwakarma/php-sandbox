<?php

 class My_Personal {

    function contact_via_email(){

    $msg = '';
    $msgClass = '';

    // Check For Submit
    if(filter_has_var(INPUT_POST,'submit')){
       // Get Form Data
       $name = htmlspecialchars($_POST['name']);
       $email = htmlspecialchars($_POST['email']);
       $message = htmlspecialchars($_POST['message']);

       // Check required files
       if(!empty($name) && !empty($email) && !empty($message)){
            // Passed
            // Check Email
            if(filter_var($email,FILTER_VALIDATE_EMAIL) === FALSE){
                // Failed
                $msg = 'Please use a valid email';
                $msgClass = 'alert-danger';
                
            } else {
                // Passed
                // Email Message Setup
                $toEmail = "dushyantss8@gmail.com";
                $subject = "Contact Request From " . $name;
                $body = "<h2>Contact Request</h2>
                         <h4>Name: </h4><p>".$name."</p>
                         <h4>Email: </h4><p>".$email."</p>
                         <h4>Message: </h4><p>".$message."</p>
                ";

                // Email Headers
                $headers = "MIME-Version: 1.0"."\r\n";
                $headers .= "Content-Type:text/html;charset=UTF-8" . "\r\n";

                // Additional Headers
                $headers .= "From: " . $name . "<" . $email . ">" . "\r\n";

                if(mail($toEmail,$subject,$body,$headers)){
                    // Email Sent
                    $msg = "Your Email was sent Successfully!";
                    $msgClass = "alert-success";
                } else {
                    // Failed to send email
                    $msg = "Sorry, your Email was not sent.";
                    $msgClass = "alert-danger";
                }
            }

       } else {
           // Failed
            $msg = 'Please fill in all fields';
            $msgClass = 'alert-danger';

       }
    }
     }
 }