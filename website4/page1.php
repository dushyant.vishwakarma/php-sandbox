<?php
    if(isset($_POST['submit'])){
        // Start the session
        session_start();

        // Set session variables
        $_SESSION['name'] = htmlentities($_POST['name']);
        $_SESSION['email'] = htmlentities($_POST['email']);

        // header() function is used to redirect to another page
        header('Location: page2.php');
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <title>PHP Sessions</title>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark"> 
    <div class="container">
        <div class="navbar-header">
            <a href="index.php" class="navbar-brand">My Website</a>
        </div>
    </div>
</nav>

<div class="container">
    <div style="margin-top:30px">

    </div>
    <h2>PHP Sessions</h2>
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
        <div class="form-group">
            <label for="name">Name: </label>
            <input type="text" name="name" class="form-control" value="" />
        </div>

        <div class="form-group">
            <label for="email">Email: </label>
            <input type="email" name="email" class="form-control" value=""/>
        </div>

        <br>

        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
    </form>
</div>


    
</body>
</html>