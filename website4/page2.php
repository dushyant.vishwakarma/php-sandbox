<?php
    // Start Session
    session_start();

    $name = $_SESSION['name'];
    $email = $_SESSION['email'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <title>PHP Sessions</title>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark"> 
    <div class="container">
        <div class="navbar-header">
            <a href="index.php" class="navbar-brand">My Website</a>
        </div>
    </div>
</nav>
    
    <div class="container">

    <div style="margin-top:30px">

    </div>
        
        <h2>PHP Session Variables</h2>

        <h5>Thank you <?php echo $name; ?>, Your email is <?php echo $email; ?> </h5>

        <button><a href="page3.php">Go to page 3</a></button>
    </div>

    
</body>
</html>