<?php

    # 1. substr() -> Returns a portion of a string
    // $output = substr('Hello',1);
    // $output = substr('Hello',-2);
    // echo $output;

    # 2. strlen() -> Returns the length of a string
    // $output = strlen('Hello World');
    // echo $output;

    # 3. strpos() -> Finds the first occurence of a sub string within an original string
    // $output = strpos('Hello World','o');
    // echo $output;

    # 4. strrpos() -> Finds the last occurence of a sub string within an original string 
    // $output = strrpos('Goodbye World','o');
    // echo $output;

    # 5. trim() -> Strips whitespaces
    // $text = "Hello My Dear Friend             ";
    // var_dump($text);                                    // 33

    // $trimmed = trim($text);
    // var_dump($trimmed);                                 // 20

    # 6. strtoupper() -> Makes everything uppercase
    // $str = strtoupper("hello world");
    // echo $str;

    # 7. strtolower() -> Makes everything lowercase
    // $str = strtolower("HELLO WORLD");
    // echo $str;

    # 8. ucwords() -> Capitalize every word
    // $str = ucwords("hello my dear friend bn d");
    // echo $str;

    # 9. str_replace() -> Replaces all occurences of a search with a replacement
    // $text = "Hello My Dear Friend!";

    // echo str_replace('Friend','BnD',$text);

    # 10. is_string() -> Checks if a given data is string or not
    // $val = 'Friend';
    // echo is_string($val);   // 1

    // $val2 = 23;
    // echo is_string($val2);  // Outputs Nothing or false


    # 11. gzcompress() -> Compress a string
    $str = "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Repellendus, ipsum! Consectetur voluptates pariatur corrupti delectus! Blanditiis veritatis quas expedita doloremque voluptatibus nam aperiam totam, similique id iste! Harum architecto ut provident animi dolor libero similique esse recusandae excepturi molestias voluptas, magnam deserunt nihil reiciendis eius reprehenderit dolorum perspiciatis ipsum ducimus?";
    
    $compressed = gzcompress($str);

    # 12. gzuncompress() -> uncompresses a string

    $original = gzuncompress($str);
