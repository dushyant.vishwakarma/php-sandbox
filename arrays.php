<?php

    #Array - Variable that holds multiple values

    /**
     * - Indexed
     * - Associative
     * - Multi-Dimensional
     */

     $people = ['Kevin','Jeremy','John'];
     // echo "<pre>";print_r($people);

     // Count the number of values in an array
     // echo count($people);

     // Associative Arrays
     $ages = ['Brad' => 23,'John' => 32,'William' => 24];
     // echo $ages['Brad'];

     // Multi-Dimensional Arrays
     $cars = [
         ['Honda',20,30],
         ['Chevy',34,35],
         ['Ford',56,66]];

     echo "<pre>";print_r($cars);
    