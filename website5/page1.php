<?php include "includes/header.php";

    // Check for Form Submission
    if(isset($_POST['submit'])){
        // Collect form data
        $username = $_POST['username'];
        $email = $_POST['email'];

        // Set cookie
        // using setcookie() function
        // function parameters => (name,value,expiration in time)
        setcookie('username',$username,time() + 3600);  // cookie will expire in 1 Hour

        // redirect to a different page
        header("Location: page2.php");
    }

?>

    <h2>PHP Cookies</h2>
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
        <div class="form-group">
            <label for="username">Username: </label>
            <input type="text" name="username" class="form-control" value="" />
        </div>

        <div class="form-group">
            <label for="email">Email: </label>
            <input type="email" name="email" class="form-control" value=""/>
        </div>

        <br>

        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
    </form>

<?php include "includes/footer.php" ?>
