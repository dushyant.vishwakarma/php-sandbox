<?php include "includes/header.php";

    if(isset($_COOKIE['username'])){
        $username = $_COOKIE['username'];
    } else {
        $msg = "User is not Set";
    }

?>

    <h2>PHP Cookies</h2>

    <?php if(isset($username)): ?>
         <p><?php echo "Hello " . $username . ", Welcome to our website!"?></p>
    <?php endif;?>


    <?php if(isset($msg)): ?>
        <p><?php echo $msg; ?></p>
    <?php endif;?>


<?php include "includes/footer.php"; ?>
