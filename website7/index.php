<?php

    $path = "/dir1/myfile.php";
    $file = "file1.txt";

    // Return File name
    // echo basename($path);

    // return file name without extension
    // echo basename($path,".php");

    // return directory name from a path
    // echo dirname($path);

    // get absolute path of any file
    // echo realpath($file);

    // checks to see if file only not any directory
    // echo is_file('test');

    // check if the file is writable
    // echo is_writable($file);

    // check if the file is readable
    // echo is_readable($file);

    // get file size
    // echo filesize($file);

    // Function to create a directory
    // mkdir("test2");

    // Function to remove/delete a directory as long as there are no files in it.
    // rmdir("test2");

    // copy file (or create and then copy)
    // echo copy($file,"file2.txt");

    // Function to rename a file
    // rename($file,"myfile.txt");

    // Function to delet a file
    // unlink("myfile.txt");

    // write from file to string
    // echo file_get_contents("file2.txt");

    // write a string to a file
    // echo file_put_contents("file2.txt","Goodbye World");    // overwrites existing contents

    // How to append data without overwriting : -

    // $current_data = file_get_contents("file2.txt");
    // // Appending
    // $current_data .= " Hello World, I'm back again!";

    // file_put_contents("file2.txt",$current_data);

    // Open file for reading
    // $handle = fopen("file2.txt","r");
    // $data = fread($handle,filesize("file2.txt"));
    // echo $data;
    // fclose($handle);        // always close the file when opening it


    // Open file for writing
    // $handle = fopen("file3.txt","w");
    // fwrite($handle,"This is written using the fwrite() function in PHP <3 ");
    // fclose($handle);


    







