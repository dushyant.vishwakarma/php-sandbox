<?php   

    // echo date('d');     // day of month
    // echo date('m');     // month
    // echo date('Y');     // Year
    // echo date('l');     // day of week

    // echo date('d/m/Y');     // day/month/year

    // echo date('H);       // Hour
    // echo date('i');      // Minute
    // echo date('s');      // Seconds
    // echo date('a');      // AM or PM

    // Set Time Zone
    // date_default_timezone_set('Asia/Kolkata');
    
    // echo date('h:i:sa');

    /**
     * UNIX Timestamp is a long integer containing the number
     * of seconds between the UNIX Epoch (January 1 1970 00:00:00 GMT) 
     * and the time specified.
     */

     // $timestamp = mktime(10,26,40,9,27,1996);

     // Now you can convert this timestamp 
     // echo date('d/m/Y',$timestamp);

     // We can also convert a string to a timestamp
     $timestamp2 = strtotime('7:00pm March 7 1998');
     echo date('m/d/Y H:i:s',$timestamp2);