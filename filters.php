<?php
    

    # FILTER_VALIDATE_BOOLEAN
    # FILTER_VALIDATE_EMAIL
    # FILTER_VALIDATE_FLOAT
    # FILTER_VALIDATE_INT
    # FILTER_VALIDATE_IP
    # FILTER_VALIDATE_REGEXP
    # FILTER_VALIDATE_URL

    # FILTER_SANITIZE_EMAIL
    # FILTER_SANITIZE_ENCODED
    # FILTER_SANITIZE_NUMBER_FLOAT
    # FILTER_SANITIZE_NUMBER_INT
    # FILTER_SANITIZE_SPECIAL_CHARS
    # FILTER_SANITIZE_STRING
    # FILTER_SANITIZE_URL

    // Check for posted data
    // if(filter_has_var(INPUT_POST,'data')){
    //     echo "Data Found";
    // } else {
    //     echo "Data Not Found";
    // }
        

    // if(filter_has_var(INPUT_POST,'data')){
    //     $email = $_POST['data'];

    //     // Remove illegal characters
    //     $email = filter_var($email,FILTER_SANITIZE_EMAIL);
    //     echo $email . '<br>';

    //     if(filter_var($email,FILTER_VALIDATE_EMAIL)){
    //         echo "Email is valid";
    //     } else {
    //         echo "Email is invalid";
    //     }
    // } 

    // $filters = [
    //     "data" => FILTER_VALIDATE_EMAIL,
    //     "data2" => [
    //         "filter" => FILTER_VALIDATE_INT,
    //         "options" => [
    //             "min_range" => 1,
    //             "max_range" => 100
    //         ]
    //     ]
    // ];

    // print_r(filter_input_array(INPUT_POST,$filters));

    $details = [
        "name" => "dushyant vishwakarma",
        "age" => 23,
        "email" => "dushyantss8@gmail.com"
    ];

    $filters = [
        "name" => [
            "filter" => FILTER_CALLBACK,
            "options" => "ucwords"
        ],
        "age" => [
            "filter" => FILTER_VALIDATE_INT,
            "options" => [
                "min_range" => 8,
                "max_range" => 30
            ],
        ],
        "email" => FILTER_VALIDATE_EMAIL
    ];

    print_r(filter_var_array($details,$filters));


    
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Filters</title>
</head>
<body>

    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
        <input type="text" name="data" placeholder="Enter Email" />
        <input type="text" name="data2" placeholder="Enter Range" />
        <input type="submit" value="Submit" />
    </form>
    
</body>
</html>