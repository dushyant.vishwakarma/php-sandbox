<?php
    // if(isset($_POST['submit'])){
    //     $name = $_POST['name'];
    //     $email = $_POST['email'];

    //     echo htmlspecialchars($name) . "<br>";
    //     echo htmlspecialchars($email) . "<br>";
    // }

    if(isset($_GET['name'])){
        $name = $_GET['name'];
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>GET and POST</title>
</head>
<body>

    <form action="get_post.php" method="POST">
        <label for="name">Name: </label>
        <input type="text" name="name" />

        <label for="email">Email: </label>
        <input type="email" name="email" />

        <input type="submit" value="Submit" name="submit" />
    </form>

    <ul>
        <li><a href="get_post.php?name=Brad">Brad</a></li>
        <li><a href="get_post.php?name=Steve">Steve</a></li>
    </ul>

    <?php if(isset($name)){ echo "{$name}'s Profile"; } ?>
    
</body>
</html>