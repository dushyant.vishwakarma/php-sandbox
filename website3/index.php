<?php

    $msg = '';
    $msgClass = '';

    // Check For Submit
    if(filter_has_var(INPUT_POST,'submit')){
       // Get Form Data
       $name = htmlspecialchars($_POST['name']);
       $email = htmlspecialchars($_POST['email']);
       $message = htmlspecialchars($_POST['message']);

       // Check required files
       if(!empty($name) && !empty($email) && !empty($message)){
            // Passed
            // Check Email
            if(filter_var($email,FILTER_VALIDATE_EMAIL) === FALSE){
                // Failed
                $msg = 'Please use a valid email';
                $msgClass = 'alert-danger';
                
            } else {
                // Passed
                // Email Message Setup
                $toEmail = "dushyantss8@gmail.com";
                $subject = "Contact Request From " . $name;
                $body = "<h2>Contact Request</h2>
                         <h4>Name: </h4><p>".$name."</p>
                         <h4>Email: </h4><p>".$email."</p>
                         <h4>Message: </h4><p>".$message."</p>
                ";

                // Email Headers
                $headers = "MIME-Version: 1.0"."\r\n";
                $headers .= "Content-Type:text/html;charset=UTF-8" . "\r\n";

                // Additional Headers
                $headers .= "From: " . $name . "<" . $email . ">" . "\r\n";

                if(mail($toEmail,$subject,$body,$headers)){
                    // Email Sent
                    $msg = "Your Email was sent Successfully!";
                    $msgClass = "alert-success";
                } else {
                    // Failed to send email
                    $msg = "Sorry, your Email was not sent.";
                    $msgClass = "alert-danger";
                }
            }

       } else {
           // Failed
            $msg = 'Please fill in all fields';
            $msgClass = 'alert-danger';

       }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <title>Contact Us</title>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-primary"> 
    <div class="container">
        <div class="navbar-header">
            <a href="index.php" class="navbar-brand">My Website</a>
        </div>
    </div>
</nav>

<div class="container">
    <div style="margin-top:30px">

    </div>
    <?php if($msg != ''): ?>
        <div class="alert <?php echo $msgClass; ?>">
            <?php echo $msg; ?>
        </div>
    <?php endif; ?>
    <h2>Contact Us</h2>
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
        <div class="form-group">
            <label for="name">Name: </label>
            <input type="text" name="name" class="form-control" value="<?php echo isset($_POST['name'])?$name : '' ?>" />
        </div>

        <div class="form-group">
            <label for="email">Email: </label>
            <input type="email" name="email" class="form-control" value="<?php echo isset($_POST['email'])?$email : '' ?>"/>
        </div>

        <div class="form-group">
            <label for="message">Message: </label>
            <textarea name="message" class="form-control" id="" cols="30" rows="10">
                <?php echo isset($_POST['message'])?$message : ''; ?>
            </textarea>
        </div>
        <br>

        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

<div style="margin-top:30px">

</div>
    
</body>
</html>