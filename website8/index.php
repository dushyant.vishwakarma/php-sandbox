<?php

    require "config/db.php";

    // Query to fetch data
    $query = "SELECT * FROM posts";

    // Get Results by executing the query
    $result = mysqli_query($conn,$query);

    // Fetch data
    $posts = mysqli_fetch_all($result,MYSQLI_ASSOC);
    // echo "<pre>";print_r($posts);

    // Free the results
    mysqli_free_result($result);

    // Close the connection
    mysqli_close($conn);
?>

<?php include "includes/header.php"; ?>
        <h1>Posts</h1>

        <?php foreach($posts as $post): ?>
            <div class="jumbotron">
                <h3><?php echo $post['title']; ?></h3>
                <small>Created On: <?php echo $post['created_at']; ?> by <?php echo $post['author']; ?></small>
                <p><?php echo $post['body']; ?></p>
                <a class="btn btn-outline-primary" href="post.php?id=<?php echo $post['id']; ?>">Read More</a>
            </div>
        <?php endforeach; ?>

<?php include "includes/footer.php"; ?>