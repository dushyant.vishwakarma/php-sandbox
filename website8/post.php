<?php

    require "config/db.php";


    // Get Post ID
    $id = mysqli_real_escape_string($conn,$_GET['id']);

    // Query to fetch data
    $query = "SELECT * FROM posts WHERE id = $id" ;

    // Get Results by executing the query
    $result = mysqli_query($conn,$query);

    // Fetch data
    $post = mysqli_fetch_assoc($result);
    // echo "<pre>";print_r($posts);

    // Free the results
    mysqli_free_result($result);

    // Close the connection
    mysqli_close($conn);
?>
<?php include "includes/header.php" ?>

            <div class="jumbotron">
                <h1><?php echo $post['title']; ?></h1>
                
                <small>Created On: <?php echo $post['created_at']; ?> by <?php echo $post['author']; ?></small>
                <p><?php echo $post['body']; ?></p>
                
            </div>
<?php include "includes/footer.php" ?>