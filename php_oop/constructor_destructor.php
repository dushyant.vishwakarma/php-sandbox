<?php

    class User {
        public $name;
        public $age;

        // Runs when an object is created
        public function __construct($name,$age){
            // Magic constants
            echo __CLASS__ . " instantiated" . "<br>";
            $this->name = $name;
            $this->age = $age;
        }

        public function sayHello(){
            return $this->name . " says Hello!";
        }

        // Destructor is called when there are no other references to a certain object
        // used for cleanup, like closing connections, etc
        public function __destruct(){
            echo "<br>";
            echo "Object Destroyed";
        }
    }

    $user1 = new User("Dushyant",23);    // constructor will run

    echo $user1->name . " is " . $user1->age . " years old";
    echo "<br>";
    echo $user1->sayHello();

    echo "<br>";
     
    $user2 = new User("John",23);    // constructor will run

    echo $user2->name . " is " . $user2->age . " years old";
    echo "<br>";
    echo $user2->sayHello();

    