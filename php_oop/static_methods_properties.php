<?php

    class User {
        public $name;
        public $age;

        public static $min_pass_length = 6;

        // static function
        public static function validate_pass($pass){
            // To call a static property, we use
            //  self::$name_of_property
            if(strlen($pass) >= self::$min_pass_length){
                return true;
            } else {
                return false;
            }
        }
    }

    // To call a static method, we use
    //  CLASS::method_name()

    $password = "hellosdfsd";
    if(User::validate_pass($password)){
        echo "Password is valid";
    } else {
        echo "Password is invalid";
    }