<?php

    // Defining a class
    class User{
        // Properties (attributes)
        public $name;
        // Methods (functions)
        public function sayHello(){
            return $this->name . " says Hello!!!";
        }
    }

    // Instantiating a user object from the user class
    $user1 = new User();

    echo $user1->name = "Dushyant";
    echo "<br>";
    echo $user1->sayHello();

    // Another user
    $user2 = new User();

    echo "<br>";
    echo $user2->name = "John";
    echo "<br>";
    echo $user2->sayHello();
