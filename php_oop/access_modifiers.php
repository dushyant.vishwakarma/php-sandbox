<?php

    class User{
        private $name;
        private $age;

        public function __construct($name,$age){
            $this->name = $name;
            $this->age = $age;
        }

        public function getName(){
            return $this->name;
        }

        public function getAge(){
            return $this->age;
        }

        public function setName($name){
            $this->name = $name;
        }

        // __get Magic Method
        public function __get($property){
            if(property_exists($this,$property)){
                return $this->$property;
            }
        }

        // __set Magic Method
        public function __set($property,$value){
            if(property_exists($this,$property)){
                $this->$property = $value;
            }

            return $this;
        }
    }

    $user1 = new User("John Doe",23);

    // echo $user1->getName() . "<br>";
    // echo $user1->getAge();

    // echo "<br><br>";

    // $user1->setName("Jeff Doe");
    // echo $user1->getName() . "<br>";

    // echo $user1->__get('name') . "<br>";
    // echo $user1->__get('age') . "<br>";

    $user1->__set('name','Nero');
    echo $user1->__get('name');



