<?php

    class User{
        protected $name;
        protected $age;

        public function __construct($name,$age){
            $this->name = $name;
            $this->age = $age;
        }
    }

    class Customer extends User{
        private $balance;

        public function __construct($name,$age,$balance){
            parent::__construct($name,$age);
            $this->balance = $balance;
        }

        public function pay($amount){
            return $this->name . " paid $" . $amount;
        }

        public function __get($property){
            if(property_exists($this,$property)){
                return $this->$property;
            }
        }
    }

    $customer1 = new Customer("Walter",50,10000000);

    // echo $customer1->pay(100);
    echo $customer1->__get('balance');