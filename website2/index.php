<?php
    include "server-info.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <title>System Info</title>
</head>
<body>

    <div class="container">
        <h2>Server & File Information</h2>

        <?php if(isset($server)): ?>
             <ul class="list-group">
                <?php foreach($server as $key => $value): ?>
                    <li class="list-group-item">
                        <strong><?php echo $key;?>: </strong> <?php echo $value; ?>
                    </li>
                <?php endforeach; ?>
             </ul>
        <?php endif; ?>
    </div>
    
</body>
</html>